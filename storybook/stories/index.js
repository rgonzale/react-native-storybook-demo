import React from 'react'
import { View, Text, StyleSheet, PixelRatio, ScrollView } from 'react-native'
import { storiesOf } from '@storybook/react-native'

const ur = PixelRatio.getFontScale() * 16
const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: ur * 0.75,
  },
  title: {
    fontSize: 1.5 * ur,
    marginBottom: 0.5 * ur,
  },
  parragraphContainer: {
    marginBottom: 3 * ur,
  },
  parragraph: {
    marginBottom: 2 * ur,
  },
  btnContainer: {
    width: '100%',
    backgroundColor: 'green',
  },
  btnText: {
    fontSize: 1.2 * ur,
    color: 'white',
    textAlign: 'center',
    paddingTop: 0.25 * ur,
    paddingBottom: 0.25 * ur,
  },
})
storiesOf('DemoApp', module)
  .add('ejemplo', () => (
    <View style={styles.container}>
      <Text style={styles.title}>Este es un titulo muy largo para probar el cambio de linea</Text>
      <ScrollView style={styles.parragraphContainer}>
        <Text style={styles.parragraph}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vulputate tempus leo, eu cursus neque eleifend et. Nullam tempus varius massa, ut posuere nibh placerat sollicitudin. Donec varius faucibus ultrices.Nulla efficitur volutpat libero a molestie. Ut ac ipsum turpis. Morbi sed vestibulum sapien, vitae rutrum ex. Ut accumsan odio et felis porta, et scelerisque tellus convallis. Sed a rutrum sem. Quisque quis placerat urna. Sed vel leo quis urna varius fermentum tempor ac eros. Donec lobortis neque ut sem pellentesque, tincidunt imperdiet lacus hendrerit. Integer a commodo ex. Vivamus porttitor eros id odio sollicitudin varius eu ac diam. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec finibus consectetur risus, vitae maximus lectus lacinia vitae. Aliquam iaculis ultricies turpis nec aliquet.Nulla efficitur volutpat libero a molestie. Ut ac ipsum turpis. Morbi sed vestibulum sapien, vitae rutrum ex. Ut accumsan odio et felis porta, et scelerisque tellus convallis. Sed a rutrum sem. Quisque quis placerat urna. Sed vel leo quis urna varius fermentum tempor ac eros. Donec lobortis neque ut sem pellentesque, tincidunt imperdiet lacus hendrerit. Integer a commodo ex. Vivamus porttitor eros id odio sollicitudin varius eu ac diam. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec finibus consectetur risus, vitae maximus lectus lacinia vitae. Aliquam iaculis ultricies turpis nec aliquet.</Text>
      </ScrollView>
      <View style={styles.btnContainer}>
        <Text style={styles.btnText}>
          OK
        </Text>
      </View>
    </View>
  ))
